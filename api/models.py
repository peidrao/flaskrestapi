from sqlalchemy import create_engine, Column, Integer, String, Boolean
from sqlalchemy.orm import relationship, scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import ForeignKey


# Configuração do Banco

engine = create_engine('sqlite:///works.db')
db_session = scoped_session(sessionmaker(autocommit=False, bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()

# Tabelas

class Person(Base):
    __tablename__='people'
    id = Column(Integer, primary_key=True)
    name = Column(String(40), index=True)
    age = Column(Integer)
    gender = Column(Boolean)

    def save(self):
        db_session.add(self)
        db_session.commit()

    def delete(self):
        db_session.delete(self)
        db_session.commit()


    def __repr__(self):
        return f'Nome: {self.name}'


class Work(Base):
    __tablename__ = 'works'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(80))
    person_id  = Column(Integer, ForeignKey('people.id'))
    person = relationship('Person')

    def save(self):
        db_session.add(self)
        db_session.commit()

    def delete(self):
        db_session.delete(self)
        db_session.commit()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(40), unique=True)
    password = Column(String(20))


    def save(self):
        db_session.add(self)
        db_session.commit()

    def delete(self):
        db_session.delete(self)
        db_session.commit()


    def __repr__(self):
        return f'Username: {self.username}'


def init_db():
    Base.metadata.create_all(bind=engine)


if __name__ == '__main__':
    init_db()

