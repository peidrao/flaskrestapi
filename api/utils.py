from models import Person

def insert():
    person = Person(name='José', age=25, gender=False)
    person.save()
    # print(person)


def get():
    person = Person.query.filter_by(name='Pedro').first()
    print(person)


def update():
    person = Person.query.filter_by(id=2).first()
    person.age = 25
    person.gender = False

    person.save()


def remove():
    person = Person.query.filter_by(id=3).first()
    person.delete()


if __name__ == '__main__':
    insert()
    # get()
    # update()
    # remove()