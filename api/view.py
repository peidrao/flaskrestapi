from flask import Flask, request
from flask_restful import Resource, Api
from models import Person, Work, User
from flask_httpauth import  HTTPBasicAuth


auth = HTTPBasicAuth()
app = Flask(__name__)
api = Api(app)

# USERS = {
#     'peidrao': '123456',
#     'maria': '123456'
# }


# @auth.verify_password
# def verification(username, password):
#     if not (username, password):
#         return False
#     return USERS.get(username) == password


@auth.verify_password
def verification(username, password):
    if not (username, password):
        return False
    return User.query.filter_by(username=username, password=password).first()


class PersonAction(Resource):

    def get(self, id):
        try:
            person = Person.query.filter_by(id=id).first()
            response = {'id': person.id, 'name': person.name, 'age': person.age, 'gender': person.gender}
        except AttributeError:
            response = {'status': 'error', 'message': 'Person not found'}
        return response

    def put(self, id):
        person = Person.query.filter_by(id=id).first()
        data = request.json
        if 'name' in data:
            person.name = data['name']
        if 'age' in data:
            person.age = data['age']
        if 'gender' in data:
            person.gender = data['gender']

        person.save()
        response = {'name': person.name, 'age': person.age, 'gender': person.gender}
        return response

    def delete(self, id):
        person = Person.query.filter_by(id=id).first()
        person.delete()
        response = {'status': 'success', 'message': 'Removed user'}
        return response



class PersonView(Resource):
    def post(self):
        data = request.json
        person = Person(name=data['name'], age=data['age'], gender=data['gender'])
        person.save()
        response = {'status': 'success', 'message': 'Created user'}
        return response

    @auth.login_required
    def get(self):
        people = Person.query.all()
        response = [{'id': p.id, 'name': p.name, 'age': p.age, 'gender': p.gender} for p in people]
        return response


class WorkView(Resource):
    def get(self):
        works = Work.query.all()
        response = [{'id': w.id, 'name': w.name, 'person': w.person.name } for w in works]
        return response

    def post(self):
        data = request.json
        try:
            person = Person.query.filter_by(id=data['person']).first()
            work = Work(name=data['name'], person=person)
            work.save()
            response = {'person': work.person.name, 'name': work.name}
        except AttributeError:
            response = {'status': 'error', 'message': 'User not found'}
        return response


api.add_resource(PersonAction, '/person/<int:id>/')
api.add_resource(PersonView, '/person/')
api.add_resource(WorkView, '/work/')

if __name__ == '__main__':
    app.run(debug=True)
