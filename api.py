import json

from flask import Flask, jsonify, request

app = Flask(__name__)

devs = [
    {'nome': 'Pedro', 'habilidades': ['Python', 'Jupyter']},
    {'nome': 'Maria', 'habilidades': ['CSS']},
    {'nome': 'André', 'habilidades': ['React']},
]

@app.route('/dev/<int:id>/', methods=['GET'])
def dev_get_id(id):
    dev = devs[id]
    return jsonify(dev)

@app.route('/dev/<int:id>/', methods=['PUT'])
def dev_put_id(id):
    data = json.loads(request.data)
    devs[id] = data
    return jsonify(data)


@app.route('/dev/<int:id>/', methods=['DELETE'])
def dev_delete_id(id):
    devs.pop(id)
    return jsonify({'status': 'Sucesso', 'message': 'Desenvolvedor removido'})


@app.route('/dev/', methods=['POST'])
def dev_create():
    data = json.loads(request.data)
    devs.append(data)
    return jsonify({'status': 'Sucesso', 'message': 'Desenvolvedor criado'})

@app.route('/devs/', methods=['GET'])
def dev_get_all():
    return jsonify(devs)


if __name__ =='__main__':
    app.run(debug=True)