from flask import Flask, json, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

devs = [
    {'id': 1, 'nome': 'Pedro', 'habilidades': ['Python', 'Jupyter']},
    {'id': 2, 'nome': 'Maria', 'habilidades': ['CSS']},
    {'id': 3, 'nome': 'André', 'habilidades': ['React']},
]


class Dev(Resource):
    def get(self, id):
        try:
            response = devs[id]
        except IndexError:
            text = f'Dev {id} not exists'
            response = {'status': 'error', 'message': text}
        return response

    def put(self, id):
        data = json.loads(request.data)
        devs[id] = data
        return data

    def delete(self, id):
        devs.pop(id)
        return {'message': 'User removed'}


class DevList(Resource):
    def get(self):
        return devs

    def post(self):
        data = json.loads(request.data)
        index = len(devs)
        data['id'] = index
        devs.append(data)
        return data



api.add_resource(Dev, '/dev/<int:id>/')
api.add_resource(DevList, '/devs/')



if __name__ == '__main__':
    app.run(debug=True)