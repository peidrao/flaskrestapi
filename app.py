from flask import Flask, json, jsonify, request
import json

app = Flask(__name__)


@app.route('/<int:id>')
def api(id):
    return jsonify({'id': id, 'nome': 'Pedro'})


@app.route('/soma/<int:valor1>/<int:valor2>')
def soma(valor1, valor2):
    return f'A soma é: {valor1+valor2}'


@app.route('/soma', methods=['POST'])
def soma_post():
    dados = json.loads(request.data)
    total = sum(dados['valores'])

    return jsonify(total)


if __name__ == '__main__':
    app.run(debug=True)